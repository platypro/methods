# Building  
The instructions below appply to both the git version and the source package. The host system requires cmake, a working C99 Compiler, and any dependencies for GLFW3.
## Step 0: Clone Submodules  
If you are building from git, the project dependencies are managed as git submodules, which allows them to be switched out for system binaries. Most modules will default to system binaries if available, even if the submodule is cloned. Submodules can be loaded by running `git submodule update --init <dependency>`. Source releases include all submodules.

| (Dependency)  | (Version)  | (System Override) |
| ------------- |:----------:|:-----------------:|
| deps/GLFW     | 3.2        | Yes               |
| deps/NanoVG   | N/A        | No                |

## Step 1: Downloading dependencies  
### Gnu/Linux  

Here is a short list of development libraries required on a gnu/linux system running Xorg:
 + xrandr
 + xinerama
 + xkb
 + xcursor

Any other libraries needed will show up as warnings in the next step.

### Windows  
For building on windows, I recommend using mingw-w64 as a toolchain. It can be installed using MSYS2 (http://www.msys2.org) by running 'Pacman -S mingw-w64-i686-gcc' for the 32bit compiler and 'pacman -S mingw-w64-x86_64-gcc' for the 64 bit one. Cmake can also be installed with 'pacman -S cmake'

## Step 2: Setting up cmake  
For building with cmake I recommend building in a new subfolder of this one. Create a new folder of any name and change directory into it. Then run the command "cmake .." to generate build files, any dependency errors will occur here.

### Cmake options  
To set any of the following options, append them to the cmake command followed by '-D' and ending with '=ON' or '=OFF'
These are case sensitive.

SysGLFW     - Use system GLFW lib where possible (default: ON)

### Hacking  
To enable debugging and to disable optimizations, just pass the argument "-DCMAKE_BUILD_TYPE=DEBUG" to cmake.

### Step 3: Building  
run 'make', and everything should show up in a subfolder named "Output"