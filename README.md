Methods Unconventional
======================
"Methods Unconventional" is a simple shooter in a fixed arena.

This game uses the same engine as Pri'sm.

_for license, see LICENCE_
_for building, see INSTALL.md_

# Gameplay  
## Running the game  
To run the game, the 'methods' command can be run. It takes no arguments.  

Once launched, press start to begin.  

Use the J,K,L, and I keys to point, space to shoot, and use W,S,D, and A to move.  
