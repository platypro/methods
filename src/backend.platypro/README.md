# Platypro's Modules

Welcome to my kludgeball!

Supported platforms (there _are_ more, but these are known to work):
	MinGW
	GNU/Linux

To use the modules, clone the entire repo into your project or pick out the modules you need. Modules have dependencies, and some modules have options. Documentation is provided inside of each header file.

To use modules in your program, include the correct CMAKE variable in your source file list. You can see the included CMakeLists.txt for all of these.

Some modules need (minimal) extra dependencies to make work

## Modules
### platypro.base - Base Modules
base.common   Common headers for all platypro.base modules

base.list     A Linked List type

base.log      Logging Module
	Depends: base.list

base.net      Networking Module
	Depends: base.list

base.strings  String/Setting Resource Library
	Depends: base.list

base.tick     Library for regular time ticks

base.tty      Tools for handling terminal functions

### platypro.game - Game-Oriented Modules
game.common   Common headers for all platypro.game modules
	Depends: base.common

game.multiplayer  Tools for handling server-side multiplayer
	Depends: base.net

game.tools        Helper functions for NanoVG and GLFW
	Note: Requires both GLFW and optionally NanoVG to be linked with your project. If NanoVG is linked with your project, USE_NANOVG needs to be defined in CFLAGS. This is included already if you use my dep-nanovg-clone repo.

game.ui           UI Tools
	Depends: game.tools (Requires NanoVG to be linked)

game.view         Game View (Scene) manager. Manages game state
	Depends: game.tools