/*
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! \defgroup platypro_game Platypro Game Library
 */

#ifndef H_PLM_GAME_TYPES
#define H_PLM_GAME_TYPES

#include <platypro.base/common.h>

//Colors!
#define COLOR_WHITE (IO_COLOR){0xFF,0xFF,0xFF,0xFF}
#define COLOR_BLACK (IO_COLOR){0x00,0x00,0x00,0xFF}
#define COLOR_RED   (IO_COLOR){0xFF,0x00,0x00,0xFF}
#define COLOR_GREEN (IO_COLOR){0x00,0xFF,0x00,0xFF}
#define COLOR_BLUE  (IO_COLOR){0x00,0x00,0xFF,0xFF}

/*! \brief General-Purpose Rectangle object
 *  Contains x, y, width, and height elements in int format
 */
typedef struct IO_Rect
{
  uint32_t x;
  uint32_t y;
  uint32_t w;
  uint32_t h;
} IO_RECT;

/*! \brief A scalable Rectangle Object
    This struct complements IO_RECT, it uses
    percentages discuised as floats, typically
    from a span of 0.0f to 1.0f and beyond
 */
typedef struct IO_FRECT
{
  float x;
  float y;
  float w;
  float h;
} IO_FLOAT_RECT;

/* \brief General-Purpose Coordinate
  An x and y property, similar to a 
  IO_RECT, except without a size
 */
typedef struct IO_Point
{
  uint32_t x;
  uint32_t y;
} IO_POINT;

/* \brief a RGBA Color
  Represents a color with R, G, and B
  elsements spanning between 0.0f and 
  1.0f. Also includes a transparency
  (alpha) element. 
 */
typedef struct IO_Color
{
  float r; 
  float g;
  float b;
  float a;
} IO_COLOR;

/* \brief a circle object.
  Represents a circle with an X, Y,
  and a radius */
typedef struct IO_CIRCLE
{
  uint32_t x;
  uint32_t y;
  uint32_t r;  
} IO_CIRCLE;

/* \brief a round object.
  Represents a circle with an X, Y,
  and both an X radius and a Y
  radius. */
typedef struct IO_ROUND
{
  uint32_t x;
  uint32_t y;
  uint32_t rx;
  uint32_t ry;
} IO_ROUND;

/* \brief A direction. Horizontal, Vertical and Diagonal. Fits in three bits. */
typedef enum Direction
{
  DIR_UP,
  DIR_LEFT,
  DIR_DOWN,
  DIR_RIGHT,
  DIR_UPLEFT,
  DIR_LEFTDOWN,
  DIR_DOWNRIGHT,
  DIR_RIGHTUP,
} DIRECTION;

#define DIRECTION_OPPOSITE(d) ((d - (d & 4 ? 4 : 0) + 2) % 4)

//! Converrs an IO_RECT into an IO_ROUND
extern IO_ROUND getRoundFromRect(IO_RECT rect);

//! Test Rect-Rect Collision
extern bool testRectCollision(IO_RECT* rect1, IO_RECT* rect2);

//! Test Rect-Round Collision
extern bool testRectRoundCollision(IO_ROUND round, IO_RECT rect);

//! Test if point (x, y) collides with rect
extern bool testPointInRect(IO_RECT rect, uint32_t x, uint32_t y);

//! Test if point (x,y) collides with round
extern bool testPointInRound(IO_ROUND round, uint32_t x, uint32_t y);

#endif