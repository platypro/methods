/*
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include "platypro.base/common.h"
#include "platypro.base/strings.h"
#include "platypro.game/tools.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include <GLFW/glfw3.h>

#ifdef USE_NANOVG
//Include nanovg 
#define NANOVG_GL2_IMPLEMENTATION
#include <nanovg.h>
#include <nanovg_gl.h>
#include <nanovg_gl_utils.h>
#endif

//The current window title
PRIVATE char* windowTitle = WINDOW_NAME;

//Last active state
PRIVATE LIBSTATE* libstate_private;

//Scancode data for use by Input_GetKeyName
KEYBIND scancodes[] = {
    {"Space",GLFW_KEY_SPACE}, {"Left",GLFW_KEY_LEFT}, {"Right",GLFW_KEY_RIGHT},
    {"Down" ,GLFW_KEY_DOWN} , {"Up",GLFW_KEY_UP}, {"Enter",GLFW_KEY_ENTER},

    {"A",GLFW_KEY_A}, {"B",GLFW_KEY_B}, {"C",GLFW_KEY_C}, {"D",GLFW_KEY_D}, 
    {"E",GLFW_KEY_E}, {"F",GLFW_KEY_F}, {"G",GLFW_KEY_G}, {"H",GLFW_KEY_H}, 
    {"I",GLFW_KEY_I}, {"J",GLFW_KEY_J}, {"K",GLFW_KEY_K}, {"L",GLFW_KEY_L}, 
    {"M",GLFW_KEY_M}, {"N",GLFW_KEY_N}, {"O",GLFW_KEY_O}, {"P",GLFW_KEY_P}, 
    {"Q",GLFW_KEY_Q}, {"R",GLFW_KEY_R}, {"S",GLFW_KEY_S}, {"T",GLFW_KEY_T}, 
    {"U",GLFW_KEY_U}, {"V",GLFW_KEY_V}, {"W",GLFW_KEY_W}, {"X",GLFW_KEY_X},
    {"Y",GLFW_KEY_Y}, {"Z",GLFW_KEY_Z}, 

    {"1",GLFW_KEY_1}, {"2",GLFW_KEY_2}, {"3",GLFW_KEY_3}, {"4",GLFW_KEY_4},
    {"5",GLFW_KEY_5}, {"6",GLFW_KEY_6}, {"7",GLFW_KEY_7}, {"8",GLFW_KEY_8},
    {"9",GLFW_KEY_9}, {"0",GLFW_KEY_0}, {NULL, 0} };

PUBLIC char* Input_GetKeyName(int code)
{
    char* result = NULL;

    KEYBIND* binding;
    for(binding = scancodes; binding->bindname; binding++)
    {
        if(binding->scancode == code)
        {
            result = binding->bindname;
            break;
        }
    }

    return result;
}

PRIVATE void errorcb(int error, const char* desc)
{
    printf("%s\n",desc);
}

#define SETBUFFCHAR(character) \
		*(libstate_private->strBuffer + libstate_private->strBufferCount) \
		= (character)

PRIVATE void character_callback(GLFWwindow* window, unsigned int codepoint)
{
  if(codepoint < 255)
  {
    SETBUFFCHAR(codepoint);
  }
  return;
}

PRIVATE void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
  if((action == GLFW_REPEAT || action == GLFW_PRESS))
  {
    if(mods == GLFW_MOD_SHIFT)
    {
      if(key == STRBUF_TAB)
      {
       SETBUFFCHAR(STRBUF_STAB); 
      }
    }
    else
    {
      switch(key)
      {
        case STRBUF_BACKSPACE: case STRBUF_TAB: case STRBUF_ENTER: case STRBUF_ESCAPE:
          SETBUFFCHAR(key);
          break;
      }
    }
  
  } 
  else if(action == GLFW_RELEASE)
  {
    if(key == STRBUF_TAB)
    {
      SETBUFFCHAR(STRBUF_UNTAB);
    }
  }
  return;
}

//Initialize GLFW, Window, Renderer, and Image Loading.
PUBLIC LIBSTATE* Window_New(IO_RECT size, uint32_t canvasW, uint32_t canvasH)
{
  LIBSTATE* result = calloc(1, sizeof(LIBSTATE));
  bool successStatus = false;
  if (result)
  {
    if(glfwInit())
    {
      result->canvasW = size.w; result->canvasH = size.h;
      result->strBufferCount = 0;
      memset(result->strBuffer, 0, sizeof(result->strBuffer));
      glfwSetErrorCallback(errorcb);

      glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
      glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);

      // Create a window, and set create it's opengl context
      result->window = glfwCreateWindow(size.w, size.h, windowTitle, NULL, NULL);
      if (result->window)
      {
        glfwSetCharCallback(result->window, character_callback);
        glfwSetKeyCallback(result->window, key_callback);
        glfwMakeContextCurrent(result->window);
        glfwSetInputMode(result->window, GLFW_STICKY_KEYS, 1);
        glfwSwapInterval(1);

        libstate_private = result;

        if(gladLoadGL())
        {
          #ifdef USE_NANOVG
          result->draw = nvgCreateGL2(NVG_ANTIALIAS | NVG_STENCIL_STROKES);
          if(result->draw)
          #endif
            successStatus = true;
        }
      }
    }
  }

  if(!successStatus) Window_Close(&result);

  return result;
}

PUBLIC bool Window_FullScreen(LIBSTATE* ls, char* state)
{
  bool oldFull = ls->isFullscreen;
  
  ls->isFullscreen = (strcmp(state, SETTINGSTR_TRUE) == 0);
  
  if(oldFull != ls->isFullscreen)
  {
    if(ls->isFullscreen)
    {
      GLFWmonitor* monitor = glfwGetPrimaryMonitor();
      const GLFWvidmode* vidmode = glfwGetVideoMode(monitor);
      glfwSetWindowMonitor(
        ls->window,
        monitor,
        0,0,
        vidmode->width,
        vidmode->height,
        GLFW_DONT_CARE);
    }
    else
    {
      glfwSetWindowMonitor(
        ls->window,
        NULL,
        0,0,
        ls->canvasW,
        ls->canvasH,
        GLFW_DONT_CARE);
    }
    return true;
  }
  return false;
}

//Adjust window ratio for drawing and input
PRIVATE LIBSTATE* Window_UpdateRatios(LIBSTATE* ls)
{
  int ww, wh;
  glfwGetWindowSize(ls->window, &ww, &wh);
  ls->scaleW = (double)ls->canvasW / (double)ww;
  ls->scaleH = (double)ls->canvasH / (double)wh;
  glViewport(0,0,ww,wh);

  return ls;
}

PRIVATE bool Window_doDraw(LIBSTATE* lstate, DRAWLOOP draw, void* data)
{
  bool result = false;
  #ifdef USE_NANOVG
  nvgBeginFrame(lstate->draw, lstate->canvasW, lstate->canvasH, 1.0f);
  #endif
  Window_Clear(lstate);
  if(draw)
    result = draw(lstate, data);
  #ifdef USE_NANOVG
  nvgEndFrame(lstate->draw);
  #endif
  return result;
}

PRIVATE bool Window_doUpdate(LIBSTATE* lstate, UPDATELOOP update, void* data)
{
  bool result = false;
  EVENTSTATE e;
  
  static int pressed = 0;
  
  e.stringBuffer = lstate->strBuffer;
  e.lastpressed = pressed;
  pressed = glfwGetMouseButton(lstate->window, GLFW_MOUSE_BUTTON_LEFT);
  e.pressed = pressed;
  glfwGetCursorPos(lstate->window, &e.x, &e.y);
  e.x = floor(e.x * lstate->scaleW);
  e.y = floor(e.y * lstate->scaleH);

  if(update)
    result = update(lstate, &e, data);
  return result;
}

PUBLIC int Window_Loop(LIBSTATE* lstate, UPDATELOOP update, DRAWLOOP draw, void* data)
{
  libstate_private = lstate;
  while (!glfwWindowShouldClose(lstate->window))
  {
    glfwSwapBuffers(lstate->window);
    Window_UpdateRatios(lstate);
    if(Window_doUpdate(lstate, update, data) || Window_doDraw(lstate, draw, data))
    {
        return true;
    }
    *(lstate->strBuffer) = 0;
    lstate->strBufferCount = 0;
    //Handle Events
    glfwPollEvents();
  }
  return false;
}

PUBLIC void Window_Clear(LIBSTATE* state)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    return;
}

PUBLIC void Window_Title(LIBSTATE* state, const char* title)
{
    windowTitle = (char*)title;
    if(state->window)
    {
        glfwSetWindowTitle(state->window, title);
    }
}

//Cleans up game, renderer, and window
PUBLIC void Window_Close(LIBSTATE** state)
{
  if(state)
  {
    #ifdef USE_NANOVG
    if((*state)->draw)
        nvgDeleteGL2((*state)->draw);
    #endif
    glfwTerminate();

    free(*state);
    *state = NULL;
  }
}

#ifdef USE_NANOVG

PUBLIC IO_TEX Draw_LoadImage(LIBSTATE* state, char* path)
{
  return nvgCreateImage(state->draw, path, 0);
}

PUBLIC void Draw_Image(LIBSTATE* state, IO_TEX image, IO_RECT imageArea)
{
  int imgw, imgh;
  NVGpaint paint;

  nvgImageSize(state->draw, image, &imgw, &imgh);
  paint = nvgImagePattern(state->draw,
      imageArea.x,imageArea.y,imageArea.w,imageArea.h, 0.0f, image, 1.0f);

  nvgBeginPath(state->draw);
  nvgRect(state->draw, imageArea.x, imageArea.y, imageArea.w, imageArea.h);
  nvgFillPaint(state->draw, paint);
  nvgFill(state->draw);
}

PUBLIC void Draw_Rect(LIBSTATE* state, IO_RECT rect)
{
    //Begin Drawing
    nvgBeginPath(state->draw);
    //Draw each rectangle
    nvgRect(state->draw, rect.x, rect.y, rect.w, rect.h);
    //Apply Color
    nvgFill(state->draw);
    return;
}

PUBLIC void Draw_Color(LIBSTATE* state, IO_COLOR color)
{
    nvgFillColor(state->draw, nvgRGBA(color.r,color.g,color.b,color.a));
    return;
}

PUBLIC void Draw_Circle(LIBSTATE* state, IO_CIRCLE circle)
{
    //Begin Drawing
    nvgBeginPath(state->draw);
    //Draw each rectangle
    nvgCircle(state->draw, circle.x, circle.y, circle.r);
    //Apply Color
    nvgFill(state->draw);
    return;
}

PUBLIC IO_FONT Draw_LoadFont(LIBSTATE* state, char* filename)
{
  nvgCreateFont(state->draw, filename, filename);
  return filename;
}

PUBLIC void Draw_Text(LIBSTATE* state, IO_FONT font, IO_POINT position, float size, char* text)
{
  nvgBeginPath(state->draw);

  nvgFontSize(state->draw, size);
  nvgFontFace(state->draw, font);
  nvgText(state->draw, position.x, position.y, text, NULL);
  return;
}

PUBLIC void Draw_Round(LIBSTATE* state, IO_RECT area)
{
	nvgBeginPath(state->draw);
	int w = (area.w / 2);
	int h = (area.h / 2);
	nvgEllipse(state->draw,
			area.x + w,
			area.y + h,
			w, h);
	nvgFill(state->draw);
	return;
}

#endif