/*
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "platypro.base/common.h"
#include "platypro.game/view.h"

#include <stdlib.h>
#include <string.h>

#include "platypro.game/tools.h"
#include "platypro.base/strings.h"

bool cleanView(void* object, unsigned int key, void* data)
{
  GAMEVIEW* v = (GAMEVIEW*)object;
  if(v->data)
    free(v->data);
  free(v);
  return true;
}

void Views_Cleanup(VIEWMANAGER** views)
{
  VIEWMANAGER* vm = *views;
  if(vm)
  {
    if(vm->floatview)
    {
      vm->floatview->clean(vm->global, vm->floatview->data);
    }

    if(vm->current)
    {
      vm->current->clean(vm->global, vm->current->data);
    }

    List_Purge(&vm->viewIndex, cleanView);
    free(vm);
    *views = NULL;
  }
}

VIEWMANAGER* Views_Init(void* global)
{
  VIEWMANAGER* result = calloc(1, sizeof(VIEWMANAGER));
  result->global = global;
  return result;
}

bool Views_Create(VIEWMANAGER* views,
    char* viewName,
    size_t dataSize,
    VIEW_INIT   init,
    VIEW_UPDATE update,
    VIEW_DRAW   draw,
    VIEW_CLEAN  clean)
{
  bool result = false;
  GAMEVIEW* viewData = List_Add_Alloc(&views->viewIndex, sizeof(GAMEVIEW), TYPE_NONE);
  if(viewData)
  {
    viewData->dataSize = dataSize;
    viewData->data = calloc(1, viewData->dataSize);
    if(viewData->data)
    {
      viewData->viewName = viewName;
      viewData->init   = init;
      viewData->update = update;
      viewData->draw   = draw;
      viewData->clean  = clean;
      result = true;
    }
  }
  return result;
}

GAMEVIEW* Views_Find(VIEWMANAGER* views, char* viewID)
{
  GAMEVIEW* v;
  foreach(v, views->viewIndex)
  {
    if(!strcmp(v->viewName, viewID))
    {
      return v;
    }
  }
  return NULL;
}

void* Views_Set(VIEWMANAGER* views, char* viewID)
{
  if(!strcmp(viewID, VIEW_BREAK))
  {
    views->doBreak = true;
  }
  else
  {
    views->next = Views_Find(views, viewID);
	return views->next->data;
  }
  return NULL;
}

void* Views_Get(VIEWMANAGER* views)
{
	if(views->floatview)
		return views->floatview->data;
	if(views->current)
		return views->current->data;
	return NULL;
}

void* Views_SetFloat(VIEWMANAGER* views, char* viewID)
{
  if(!views->floatview
      || strcmp(views->current->viewName, viewID)
      || (views->next && strcmp(views->next->viewName, viewID)))
  {
    views->floatview = Views_Find(views, viewID);
    views->floatview->init(views->global, views->floatview->data);
  }
  return views->floatview->data;
}

bool Views_CloseFloat(VIEWMANAGER* views)
{
  GAMEVIEW** f = &views->floatview;
  if(*f)
  {
    (*f)->clean(views->global, (*f)->data);
    memset((*f)->data, 0, (*f)->dataSize);
    views->floatview = NULL;
    return true;
  }
  return false;
}

PRIVATE bool updateView(VIEWMANAGER* view)
{
  bool result = false;
  //Update view
  if(view->next)
  {
    if(view->current)
    {
      view->current->clean(view->global, view->current->data);
      memset(view->current->data, 0, view->current->dataSize);
    }

    view->current  = view->next;
    view->next     = NULL;

    if(view->current->data)
    {
      if ( !strcmp(view->current->init(view->global, view->current->data), VIEW_BREAK) )
        result = true;
    }
  }
  return result;
}

bool Views_Draw(LIBSTATE* ls, void* _view)
{
  VIEWMANAGER* view = (VIEWMANAGER*)_view;
  bool result;
  
  nvgResetTransform(ls->draw);
  result = (!strcmp(view->current->draw(view->global, view->current->data), VIEW_BREAK));

  if(result) return result;
  if(view->floatview)
  {
    nvgResetTransform(ls->draw);
    Draw_Color(ls, (IO_COLOR){0x00,0x00,0x00,0x99});
    Draw_Rect(ls, (IO_RECT){0,0,ls->canvasW,ls->canvasH});
    result = (!strcmp(view->floatview->draw(view->global, view->floatview->data), VIEW_BREAK));
  }

  return result;
}

bool Views_Update(LIBSTATE* ls, EVENTSTATE* e, void* _view)
{
  VIEWMANAGER* view = (VIEWMANAGER*)_view;
  bool result = updateView(view);
  
  if(view->doBreak)
  {
    return true;
  }

  if(view->floatview)
  {
    view->floatview->update(view->global, view->floatview->data, e);
    e = NULL;
  }
  
  //--Update--//
  char* nextview = view->current->update(view->global, view->current->data, e);
  if(!strcmp(nextview, VIEW_BREAK))
    return true;

  if(!strcmp(nextview, VIEW_CONTINUE))
      return false;

  if(!view->next)
    view->next = Views_Find(view, nextview);
  
  updateView(view);

  return result;
}
