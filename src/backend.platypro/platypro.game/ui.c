/*
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "platypro.base/common.h"
#include "platypro.game/ui.h"

#include "platypro.base/strings.h"

#include <stdlib.h>
#include <string.h>

UI_STYLE defaultStyle = {
    (IO_COLOR){0x00,0x00,0xDD,0xFF},
    (IO_COLOR){0x45,0x45,0x99,0xFF},
    (IO_COLOR){0xFF,0xFF,0xFF,0xFF},
    (IO_COLOR){0x00,0x00,0x99,0xFF},
    (IO_COLOR){0x00,0x00,0x66,0xFF},
    (IO_COLOR){0xBF,0xBF,0xBF,0xFF}
};

typedef struct UpdatePass
{
  EVENTSTATE* e;
  void* state;
} UI_UPDATEPASS;


int UI_Object_Create(UI_INDEX* ui, struct UI_Def object)
{
  UI_ELEMENT* e = List_Add_Alloc(&ui->uiObjects, sizeof(UI_ELEMENT), object.type);
  if(e)
  {
    ui->indexAt++;

    strncpy(e->name, object.name, MAX_STRING_LEN);
    e->event = object.event;
    e->dimens     = object.dimens;
    strncpy(e->text, object.text, MAX_STRING_LEN);
    e->parentIndex = ui;
    e->index = ui->indexAt;
    return e->index;
  }
  return -1;
}

void UI_Object_Destroy(UI_INDEX* ui, int object)
{
  UI_ELEMENT* objectToDestroy
    = UI_Object_Get(ui, object);
  if(objectToDestroy)
    List_Remove(&ui->uiObjects, objectToDestroy);

  return;
}

void UI_Object_DestroyAll(UI_INDEX* ui)
{
  List_Purge(&ui->uiObjects, NULL);
  ui->focus = NULL;
}

UI_ELEMENT* UI_Object_Get(UI_INDEX* ui, int object)
{
  UI_ELEMENT* result = NULL;
  UI_ELEMENT* e;
  foreach(e, ui->uiObjects)
  {
    if(e->index == object)
    {
       result = e;
       break;
    }
  }
  return result;
}

UI_ELEMENT* UI_Object_GetByName(UI_INDEX* ui, char* ename)
{
  UI_ELEMENT* result = NULL;
  UI_ELEMENT* e;
  foreach(e, ui->uiObjects)
  {
    if(!strcmp(e->name, ename))
    {
       result = e;
       break;
    }
  }
  return result;
}

bool UI_Object_SetText(UI_INDEX* ui, int index, char* text)
{
  UI_ELEMENT* object = UI_Object_Get(ui, index);
  strncpy(object->text, text, MAX_STRING_LEN);
  return true;
}

void UI_Destroy(UI_INDEX** index)
{
  if(index && *index)
  {
    UI_INDEX* i = *index;
    if(i->uiObjects)
      List_Purge(&i->uiObjects, NULL);
    if(i->style && i->style != &defaultStyle)
    {
      memset(i->style, 0, sizeof(UI_STYLE));
      free(i->style);
    }
    free(*index);
    *index = NULL;
  }
  return;
}

UI_INDEX* UI_Create(LIBSTATE* libs, UI_STYLE* style, IO_FONT font)
{
  bool success = false;
  UI_INDEX* result = calloc(1, sizeof(UI_INDEX));
  if(result)
  {
    result->state = libs;
    result->font  = font;
    if(!style)
    {
      result->style = &defaultStyle;
      success = true;
    }
    else
    {
      result->style = calloc(1, sizeof(UI_STYLE));
      if(result->style)
      {
        memcpy(result->style, style, sizeof(UI_STYLE));
        success = true;
      }
    }
  }

  if(!success)
  {
    UI_Destroy(&result);
  }

  return result;
}

PUBLIC void UI_Object_Auto(UI_INDEX* ui, UI_DEF* uiElements, size_t uiElementsSize, LIST stringfile)
{
  int i;
  for(i = 0;i < uiElementsSize / sizeof(UI_DEF);i++)
  {
    uiElements[i].text = Strings_Get(stringfile, uiElements[i].text);
    UI_Object_Create(ui, uiElements[i]);
  }
  return;
}

PRIVATE bool changeHover(UI_ELEMENT* e, EVENTSTATE* events)
{
  bool result = false;
  bool cursorColliding = testPointInRect(e->dimens, events->x, events->y);
  if(e->hoverState == UIHOVER_ROLLOVER && events->pressed)
  {
    e->hoverState = UIHOVER_PRESS;
  }

  if(e->hoverState == UIHOVER_PRESS && !events->pressed)
  {
    result = cursorColliding;
    e->hoverState = UIHOVER_DEFAULT;
  }

  if(cursorColliding)
  {
    if(e->hoverState == UIHOVER_DEFAULT && !events->pressed)
    {
      e->hoverState = UIHOVER_ROLLOVER;
    }
  } else e->hoverState = UIHOVER_DEFAULT;
  return result;
}

bool UI_Focus(UI_INDEX* ui, UI_ELEMENT* object)
{
  if(ui->focus != object && ui->focus && *ui->textcache != '\000')
  {
    if(*ui->focus->text == '\000')
      strcpy(ui->focus->text, ui->textcache);
    strcpy(ui->textcache, "");
  }
  
  ui->focus = object;
  return true; 
}

bool UI_doUpdate(void* object, unsigned int key, void* data)
{
  UI_ELEMENT* e = object;
  UI_UPDATEPASS* up = data;
  //State Detection

  if(changeHover(e, up->e))
  {
    //If a checkbox
    if(key == UITYPE_CHECK)
    {
      if(*e->text == *UI_FALSE)
      {
        *e->text = *UI_TRUE;
      }
      else
      {
        *e->text = *UI_FALSE;
      }
    }
    
    UI_Focus(e->parentIndex, e);
    
    if(key == UITYPE_TEXTBOX && *e->parentIndex->textcache == '\000')
    {
      strcpy(e->parentIndex->textcache, e->text);
      strcpy(e->text, "");
    }
    
    if(e->event)
    {
      if(!e->event(e->parentIndex, e, up->state)) return false;
    }
  }

  if(e->parentIndex->focus == e)
  {
    unsigned short* buffCursor = up->e->stringBuffer;

    while(*buffCursor != '\000')
    {
      UI_ELEMENT* i, *mark;
      if(key == UITYPE_TEXTBOX)
      {
        size_t textlen = strlen(e->text);
        if(*buffCursor == STRBUF_BACKSPACE)
        {
          if(textlen == 0)
          {
            strcpy(e->text, e->parentIndex->textcache);
            strcpy(e->parentIndex->textcache, "");
          }
          
          if(strlen(e->text) > 0)
          {
            *(e->text + strlen(e->text) - 1) = '\000';
            if(e->event)
              e->event(e->parentIndex,e, up->state);
          }
        }
        if(*buffCursor < 256)
        {
          if(textlen < (MAX_STRING_LEN - 1))
          {
            *(e->text + textlen) = *buffCursor;
            *(e->text + textlen + 1) = '\000';
            if(e->event)
              e->event(e->parentIndex, e, up->state);
          }
        }
      }
      
      switch(*buffCursor)
      {
       case STRBUF_ENTER: case STRBUF_TAB:
        mark = NULL;
        //Find Current Object
        foreach(i, e->parentIndex->uiObjects)
        {
          if(!mark)
          {
            if(i == e)
            {
              mark = i;
            }
          }
          else
          {
            if(*buffCursor == STRBUF_ENTER)
            {
              if(LIST_GETTYPE(FORELEMENT) == UITYPE_BTN)
              {
                i->event(i->parentIndex, i, up->state);
                break;
              } 
            }
            else if(*buffCursor == STRBUF_TAB)
            {
              if(LIST_GETTYPE(FORELEMENT) == UITYPE_TEXTBOX)
              {
                UI_Focus(i->parentIndex, i);
                break;
              } 
            }
          }

          if(!LIST_GETNEXT(FORELEMENT))
          {
            FORELEMENT = e->parentIndex->uiObjects; 
          }
        }
        break; 
      }
      
      *buffCursor = '\000';
      buffCursor++;
    }
  }

  return true;
}

PRIVATE IO_COLOR invert_Color(IO_COLOR* c)
{
  IO_COLOR result;
  result.r = 0xFF - c->r;
  result.g = 0xFF - c->g;
  result.b = 255.0f - c->b;
  return result;
}

bool UI_doDraw(void* object, unsigned int key, void* data)
{
  UI_INDEX* ls  = (UI_INDEX*)data;
  UI_ELEMENT* e = object;

  IO_COLOR color;

  if(key == UITYPE_LABEL)
    color = ls->style->labelColor;
  else if(e->parentIndex->focus == e && key == UITYPE_TEXTBOX)
    color = ls->style->focusedColor;
  else
  //Decide color scheme
  switch(e->hoverState)
  {
  case UIHOVER_DEFAULT:
    if(key == UITYPE_TEXTBOX)
      color = ls->style->textboxColor;
    else
      color = ls->style->buttonColor;
    break;
  case UIHOVER_ROLLOVER:
      color = ls->style->hoverColor;
    break;
  case UIHOVER_PRESS:
      color = ls->style->pressColor;
    break;
  }

  //Draw the UI Object
  Draw_Color(ls->state, color);
  nvgBeginPath(ls->state->draw);
  nvgRoundedRect(ls->state->draw, 
    e->dimens.x, 
    e->dimens.y, 
    e->dimens.w, 
    e->dimens.h, 
    5);
  nvgFill(ls->state->draw);
  color = invert_Color(&color);
  color.a = 0xFF;

  //Draw Checkbox status (if applicable)
  if(key == UITYPE_CHECK)
  {
    IO_RECT checkRect =
    {
        e->dimens.x + e->dimens.w / 2 - e->dimens.h / 2 + CHECKBOX_PADDING,
        e->dimens.y + CHECKBOX_PADDING,
        e->dimens.h - (2 * CHECKBOX_PADDING),
        e->dimens.h - (2 * CHECKBOX_PADDING)
    };

    if(*e->text == *UI_TRUE)
    {
      Draw_Color(ls->state, COLOR_GREEN);
    }
    else
    {
      Draw_Color(ls->state, COLOR_RED);
    }
    Draw_Rect(ls->state, checkRect);
  }
  else
  {
    IO_POINT textPoint = {
          e->dimens.x + (e->dimens.w / 2),
          e->dimens.y + (e->dimens.h / 2)} ;

    nvgTextAlign(ls->state->draw, NVG_ALIGN_CENTER | NVG_ALIGN_MIDDLE);
    
    if(*ls->textcache != '\000' && *e->text == '\000')
    {
      Draw_Color(ls->state, ls->style->labelColor);
      Draw_Text(ls->state, ls->font,
        textPoint, UIFONTSIZE, ls->textcache);
    } 
    else
    {
      Draw_Color(ls->state, color);
      Draw_Text(ls->state, ls->font,
        textPoint, UIFONTSIZE, e->text);
    }
  }

  return true;
}

bool UI_Update(UI_INDEX* ui, EVENTSTATE e, void* state)
{
  UI_UPDATEPASS p = {&e, state};
  if(ui->uiObjects)
  List_Map(&ui->uiObjects, UI_doUpdate, &p);
  return false;
}

bool UI_Draw(UI_INDEX* ui)
{
  if(ui->uiObjects)
  List_Map(&ui->uiObjects, UI_doDraw, ui);
  return false;
}
