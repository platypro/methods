/*
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#ifndef H_PLM_GAME_TOOLS
#define H_PLM_GAME_TOOLS

/** \file tools.h

    Various tools for working with GLFW and nanovg

 *  \defgroup platypro_game_tools Tools
 *  \ingroup platypro_game
 */
/** @{ */
#include "glad/glad.h"
#include <GLFW/glfw3.h>
#ifdef USE_NANOVG
#include <nanovg.h>
#endif

//General headers
#include "platypro.base/common.h"
#include "platypro.game/common.h"

//! Default window title
#define WINDOW_NAME "Untitled Window"

typedef int       IO_TEX;
typedef char*     IO_FONT;

#define MAX_STRINGBUF_LEN 5
#define STRBUF_BACKSPACE GLFW_KEY_BACKSPACE
#define STRBUF_TAB       GLFW_KEY_TAB
#define STRBUF_ENTER     GLFW_KEY_ENTER
#define STRBUF_ESCAPE    GLFW_KEY_ESCAPE
#define STRBUF_UNTAB     1022
#define STRBUF_STAB      1023

typedef struct LibState
{
  GLFWwindow* window;

  int strBufferCount; //Current position in strBuffer
  unsigned short strBuffer[MAX_STRINGBUF_LEN];

  bool isFullscreen;

  int canvasW;
  int canvasH;

  double scaleW;
  double scaleH;

  #ifdef USE_NANOVG
    NVGcontext* draw;
  #endif

} LIBSTATE;

typedef struct EventState
{
  unsigned short* stringBuffer;

  double x;
  double y;
  
  char pressed;
  char lastpressed;
} EVENTSTATE;

typedef int  (*DRAWLOOP)(LIBSTATE* state, void* data);
typedef int  (*UPDATELOOP)(LIBSTATE* state, EVENTSTATE* e, void* data);
typedef void (*DRAWFUN)(LIBSTATE* state, void* data);

//! Structure for storing keybind data
typedef struct KeyBind {
	char* bindname;
	int scancode;
} KEYBIND;

//!Gets the name of a key from a GLFW Keycode
extern char* Input_GetKeyName(int code);

//Window Functions
/*! Creates a new window of size 'area'.
    \returns the success of the window opening
 */
extern LIBSTATE* Window_New(IO_RECT size, uint32_t canvasW, uint32_t canvasH);

//! Close the window as soon as possible
extern void Window_Close(LIBSTATE** state);

//! Set the title of the window
extern void Window_Title(LIBSTATE* state, const char* title);

//! This function calls 'loop' until the window is closed
extern int Window_Loop(LIBSTATE* state, UPDATELOOP update, DRAWLOOP draw, void* data);

//! Toggle fullscreen status on window
extern bool Window_FullScreen(LIBSTATE* ls, char* state);

//! This function clears the window's buffer
extern void Window_Clear(LIBSTATE* state);

#ifdef USE_NANOVG

//! Draw a rectangle object
extern void Draw_Rect(LIBSTATE* state, IO_RECT rect);

//! Set the drawing color
extern void Draw_Color(LIBSTATE* state, IO_COLOR color);

//! Draw a circle object
extern void Draw_Circle(LIBSTATE* state, IO_CIRCLE circle);

//! Draw a round object
extern void Draw_Round(LIBSTATE* state, IO_RECT area);

//! Load an image
extern IO_TEX Draw_LoadImage(LIBSTATE* state, char* path);

//! Draw an image
extern void Draw_Image(LIBSTATE* state, IO_TEX image, IO_RECT imageArea);

//! Load a font
extern IO_FONT Draw_LoadFont(LIBSTATE* state, char* filename);

//! Draw text
extern void Draw_Text(LIBSTATE* state, IO_FONT font, IO_POINT position, float size, char* text);

#endif

/** @} */
#endif
