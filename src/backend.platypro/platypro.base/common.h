//General Headers

/*! \defgroup platypro_base Platypro Base Library
 */

#ifndef INC_GENERAL_H_
#define INC_GENERAL_H_

//Definition for bool value
#define bool int
//True and False Definitions
#define false 0
#define true !false //!< It's funny because it's true

//Public and Private Definitions
#define PUBLIC
#define PRIVATE static

#define NUMELEMENTS(array) sizeof(array) / sizeof(*array)

#ifndef NULL
#define NULL 0
#endif

#include <stdint.h>

#endif
