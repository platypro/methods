/* 
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include "platypro.base/common.h"
#include "platypro.base/tty.h"

#ifdef _WIN32
  #include <conio.h>

  bool Term_Init()
  { return true; }

  bool Term_Cleanup()
  { return true; }

  char Term_Getch()
  {
   if(_kbhit())
    return _getch();
   else return 0;
  }
#else
  #include <errno.h>
  #include <unistd.h>
  #include <termios.h>
  #include <fcntl.h>

  static struct termios oldt;

  bool Term_Init()
  {
    bool result = true;
    int flags;
    struct termios term;

    //Change term settings
    if (tcgetattr( STDIN_FILENO, &oldt) == 0)
    {
      term  = oldt;
      term.c_lflag &= ~(ICANON | ECHO | ISIG);
      if ( tcsetattr( STDIN_FILENO, TCSANOW, &term) != 0 )
        result = false;
    } else result = false;
    
    //Disable blocking
    flags = fcntl(STDIN_FILENO, F_GETFL, 0);
    fcntl(STDIN_FILENO, F_SETFL, flags | O_NONBLOCK);
    
    return result;
  }

  bool Term_Cleanup()
  {
    bool result =  (tcsetattr( STDIN_FILENO, TCSANOW, &oldt) == 0);

    return result;
  }

  char Term_Getch()
  {
     char buff = '\000';
     read(STDIN_FILENO, &buff, 1);
     return buff;
  }
#endif