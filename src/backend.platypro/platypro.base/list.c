/* 
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include "platypro.base/common.h"
#include "platypro.base/list.h"

#include <string.h>
#include <stdlib.h>

PRIVATE bool List_DefaultClean(void* object, LISTTYPE key, void* data)
{
    free(object);
    return true;
}

PUBLIC LISTITEM* List_FindLast(LIST origin)
{
  return origin->elder->last;
}

PRIVATE bool List_Reg(LIST* list, LISTITEM* add)
{
  if(list)
  {
    if(!*list)
    {
      *list = add;
      add->elder = calloc(1, sizeof(LISTHEADER));
      if(!add->elder) return false;
    }

    LISTHEADER* elder = (*list)->elder;

    if(!elder->first)
    {
      elder->first = add;
      elder->last  = add;
    }
    else
    {
      elder->last->sibling = add;
      elder->last = add;
    }
    add->elder = elder;
    elder->size++;
  } 
  add->sibling = NULL;

  return true;
}

//If list is null, create a new list
PUBLIC bool List_Add(LIST* list, void* object, LISTTYPE header)
{
    LISTITEM* result = calloc(1, sizeof(LISTITEM));
    if(result)
    {
        result->objType = header;
        result->object  = object;
        List_Reg(list, result);
    }
    *list = result->elder->first;
    return true;
}

PUBLIC void* List_Add_Alloc(LIST* list, size_t size, LISTTYPE header)
{
    void* object = calloc(1, size);
    LISTITEM* newlist = calloc(1, sizeof(LISTITEM));
    if(object && newlist)
    {
      newlist->objType = header;
      newlist->object  = object;
      List_Reg(list, newlist);
    }
    return object;
}

PUBLIC LISTITEM* List_Promote(LIST* list, void* object, LISTTYPE header)
{
  LISTITEM* find;
  LISTITEM* last = (*list)->elder->first;
  LISTITEM* c = (*list)->elder->first;
  
  //Find list item
  while(c->object != object || c->objType != header)
  {
    last = c;
    c = c->sibling;
    if(!c) return NULL;
  }
  
  //Take note of the found list item
  find = c;
  
  //If object is not at the end of the list
  if (c->sibling)
  {
    //Remove list entry from list
    if (last != c)
    {
      last->sibling = c->sibling;
    }
    else
    {
      (*list)->elder->first = find->sibling;
    }
    
    //Find end of list
    while (c->sibling)
    {
      c = c->sibling;
    }
    
    //Insert found entry back into list
    c->sibling = find;
    find->sibling = NULL;
  }
  
  return find;
}

PUBLIC bool List_Sort(LIST* list, LCOMPAR compar)
{
  int sorted   = true;

  do
  {
    sorted = true;
    LISTITEM* listptr = *list;
    LISTITEM* last    = NULL;

    while(listptr && listptr->sibling)
    {
      int diff = compar(listptr->object, listptr->sibling->object);

      if(diff < 0)
      {
        LISTITEM* sibl = listptr->sibling;
        //Swap
        if(!last) 
        {
          (*list)->elder->first = sibl;
          *list = sibl;
        }
        else last->sibling = sibl;

        listptr->sibling = sibl->sibling;
        sibl->sibling = listptr;
        sorted = false;
      }

      last = listptr;
      listptr = listptr->sibling;
    }
  } while(!sorted);
  
  return sorted;
}

PRIVATE void List_InternalRemove(LIST* list, void* test)
{
  LISTHEADER* head = (*list)->elder;
  LISTITEM* i = head->first;

  if(head->first->object == test)
  {
    if(*list == head->first) *list = (*list)->sibling;
    head->first = head->first->sibling;
  } 
  else while(i->sibling)
  {
    if(i->sibling->object == test)
    {
      LISTITEM* trash = i->sibling;
      if(*list == i->sibling) *list = i;

      if(i->sibling == head->last)
      {
        head->last = i;
        i->sibling = NULL;
      }
      else
      {
        i->sibling = i->sibling->sibling;
      }

      memset(trash, 0, sizeof(LISTITEM));
      free(trash);

      break;
    }
    i=i->sibling;
  }

  head->size--;
  if(head->size == 0)
	  free(head);
  
  return;
}

PUBLIC bool List_RemoveEntry(LIST* list, LISTITEM* entry)
{
  List_InternalRemove(list, entry->object);
  return true;
}

PUBLIC bool List_Remove(LIST* list, void* object)
{
  if(*list)
  List_InternalRemove(list, object);
  return true;
}

PUBLIC int List_Count(LIST* list)
{
  if(*list)
	  return (*list)->elder->size;
  else return 0;
}

PUBLIC bool List_Map(LIST* list, LISTMOD map, void* data)
{
  bool result = true;
  if(*list && map)
  {
    LISTITEM *current, *next;
    for(current = (*list)->elder->first; current; current=next)
    {
      next = current->sibling;
      result = map(current->object, current->objType, data);
      if (!result) break;
    }
  }
  return result;
}

PUBLIC bool List_Merge(LIST* append, LIST* in)
{
	if(!(*append))
	{
		*append = *in;
	}
	else
	{
    LISTITEM* i = (*in)->elder->first;
    free((*in)->elder);
    (*append)->elder->last = (*in)->elder->first;
    while(i)
    {
      i->elder = (*append)->elder;
      i = i->sibling;
    }
	}
	return true;
}

PUBLIC bool List_Clear(LIST* list)
{
    bool result = true;

    if(*list)
    {
      while(*list)
      {
        result = List_Remove(list, (*list)->elder->first->object);
      }
    }

    return result;
}

PUBLIC bool List_Purge(LIST* list, LISTMOD destroy)
{
    bool result = true;
    if(destroy) result = List_Map(list, destroy, NULL);
    else        result = List_Map(list, List_DefaultClean, NULL);
    result = List_Clear(list);
    return result;
}

PUBLIC int List_FindIndex(LIST* list, void* object)
{
  int result = 0;
  LISTITEM* at = (*list)->elder->first;
  
  while(at)
  {
    if(at->object == object)
    {
        return result;
    }
    at=at->sibling;
    result ++;
  }

  return -1;
}

PUBLIC void* List_GetI(LIST* list, int index)
{
    void* result = NULL;

    LISTITEM* at = (*list)->elder->first;
    int i = 0;
    while(at)
    {
        if(i == index) 
        { 
            result = at->object; 
            break; 
        }
        at=at->sibling; i++;
    }

    return result;
}

PRIVATE LISTITEM* List_GetO(LIST* list, void* object)
{
    LISTITEM* result = (*list)->elder->first;
    while(result)
    {
        if(result->object == object) break;
        result=result->sibling;
    }
    return result;
}

PRIVATE void** List_FindQ(LIST* list, LISTTYPE query, void* header)
{
    void** result = NULL;

    if(list){
        LISTITEM* at;
        if(header) at = List_GetO(list, header)->sibling;
        else       at = (*list)->elder->first;

        while(at)
        {
            if(at->objType == query) 
            {
                result = &at->object;
                break;
            }
            at=at->sibling;
        }
    }

    return result;
}

PUBLIC void* List_GetQ(LIST* list, LISTTYPE query, void* header)
{
    void** result = List_FindQ(list, query, header);
    if(result)
    {
        return *result;
    } else return NULL;
}

PUBLIC bool List_Reset(LIST* list, LISTTYPE query, void* object, void* header)
{
    void** listItem = List_FindQ(list, query, header);
    *listItem = object;
    return true;
}
