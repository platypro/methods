/*
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_PLM_BASE_TICK
#define H_PLM_BASE_TICK

/** \file tick.h
 
    This file is responsible for defining
    the program tick API.

 *  \defgroup platypro_tick Tick
 *  \ingroup platypro_base
 */
/** @{ */

#if _WIN32
#include <windows.h>
typedef LARGE_INTEGER TICK_OBJ;
#else
#include <time.h>
typedef struct timespec TICK_OBJ;
#endif

#define TICKS_PERSEC 50
#define TICK_LEN 0.02

/*! Find ticks since last call. In code, define a TICK_OBJ, 
 *  then continually pass its reference into this function.
 *  \param tick    Tick info
 *  \returns Tick count since last called
 */
extern int Tick_Get(TICK_OBJ* tick);

/** @} */
#endif