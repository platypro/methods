/* 
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#ifndef H_PLM_BASE_LIST
#define H_PLM_BASE_LIST

/** \file list.h

    List API.

 *  \defgroup platypro_list Dynamic List API
 *  \ingroup platypro_base
 */
/** @{ */

//Library import headers
#include <stdio.h>

//General Headers
#include "platypro.base/common.h"

#define TYPE_NONE 1
#define TYPE_(num) num + 2

typedef unsigned int LISTTYPE;

//! List compare function
typedef int (*LCOMPAR) (void* obj1, void* obj2);

struct ListHeader;

/*! \brief A dynamic list type
 *
    This type stores pointer data.
    It must be initialized to NULL, or the
    manipulation functions will NOT work.
  
    One instance of this object 
    represents one list item
 */
typedef struct ListItem {
	struct ListHeader* elder;   //!< Points to the first object in the list
	struct ListItem* sibling; //!< Points to the next object in the list

	LISTTYPE objType;     //!< Character for determining the type of 'object'
	void* object;     //!< Contains the object data for this list item
} LISTITEM;

typedef struct ListHeader
{
    LISTITEM* first;
    LISTITEM* last;
    uint16_t size;

} LISTHEADER;

typedef LISTITEM* LIST;

#define _LIST(l) (l)->elder
#define _LISTITEM(l) (l)->first

//! Retrieve the type from the specified list object
#define LIST_GETTYPE(list) (list)->objType
//! Retrieve the object from the specified list object
#define LIST_GETOBJ(list) (list)->object
//! Retrieve the sibling of the specified list object
#define LIST_GETNEXT(list) (list)->sibling

//! A list definition
//typedef LISTITEM* LIST;

//! The list item used in "foreach". It can be used to manipulate the list directly.
#define FORELEMENT element    

/*! \brief       Loops through all items present in a list
 *  \param item  The item to assign while looping
 *  \param list  The list to loop through
 */
#define foreach(item, list) \
    LISTITEM* FORELEMENT; \
    if(list) for(FORELEMENT = (LISTITEM*)list, item=FORELEMENT->object; FORELEMENT; \
        FORELEMENT = FORELEMENT->sibling, item=FORELEMENT ? FORELEMENT->object : NULL)

//! List modification template
typedef bool (*LISTMOD)(void* object, LISTTYPE key, void* data);

/*! \brief Finds the last list item
 *  \returns the item.
 */
extern LIST List_FindLast(LIST origin);

/*! \brief Moves a list item to the end of a list.
 *  \param list       A pointer to the list to be manipulated.
 *  \param object     The object to promote
 * 
 *  \returns A list object representing the object (if found)
 */
extern LIST List_Promote(LIST* list, void* object, LISTTYPE header);

/*! \brief Sort a list
 *  \param list       A pointer to the list to be manipulated.
 *  \param compar     Function for comparing list items, works similar to qsort comar()
 */
extern bool List_Sort(LIST* list, LCOMPAR compar);

/*! \brief Adds an item to a list
 	       The list should be passed to List_Destroy by the end of execution
 *  \param list        A pointer to the list the object needs to be added to
 *  \param object      The pointer to be added to the list
 *  \param header      A one-character header for determining object type
 *
 *  \returns success of add
 */
extern bool List_Add (LIST* list, void* object, LISTTYPE header);

/*! \brief Inserts an item to a specific point in the list
 *  \param list        A pointer to the list the object needs to be added to
 *  \param object      The pointer to be added to the list
 *  \param header      A one-character header for determining object type
 *  \param index       The point to add the object to the list
 */
extern bool List_Insert (LIST* list, void* object, LISTTYPE header, int index);

/*! \brief Adds and allocates an item to a list
 *  \param list        A pointer to the list the object needs to be added to.
 *  \param size        The size of the new object
 *  \param header      A one-character header for determining object type
 *
 *  \returns the new object, as allocated
 */
extern void* List_Add_Alloc (LIST* list, size_t size, LISTTYPE header);

/*! \brief Removes an item from a list
 *  \param list        A pointer to the list the object needs to be removed from.
 *  \param object      The object to be removed, the function finds this in the list for you.
 *
 *  \returns success of removal
 */
extern bool List_Remove(LIST* list, void* object);

/*! \brief Removes a list entry given another list reference
 *  \param list        A pointer to the list the object needs to be removed from.
 *  \param entry       The list entry to be removed.
 */
extern bool List_RemoveEntry(LIST* list, LISTITEM* entry);

//! Counts the items in a list
extern bool List_Count(LIST* list);

/*! \brief Applies a function to a list, and then clears it
 *  \param list        A pointer to the list to clear
 *  \param destroy     A function pointer for objects with custom cleanup functions. If NULL is passed here, the function tries calling free() for each item
 *
 *  \returns success of clear
 *
 *  All lists must be cleared before program termination.
 */
extern bool List_Purge(LIST* list, LISTMOD destroy);

/*! \brief Applies a function to all the items of the list
 *	\param list       A pointer to the list to manipulate
 *  \param mapfun     A callback to be executed for each item
 *  \param data       Any additional data needed to be sent to mapfun
 */
extern bool List_Map(LIST* list, LISTMOD mapfun, void* data);

/*! \brief Combindes two lists
   		List 'in' no longer needs to be cleared by end of execution, since it is now part of list 'append'.
 *  \param append     List to append to
 *  \param in         List to add
 */
extern bool List_Merge(LIST* append, LIST* in);

/*! \brief Clears list items without using a clean function
 *  \param list       A pointer to the list to clear
 */
extern bool List_Clear(LIST* list);

/*! \brief Gets the index of the specified item
 *	\param list       A pointer to the list to observe
 *  \param object     The object to find
 *  \returns          The index found, -1 if not found
 */
extern int List_FindIndex(LIST* list, void* object);

/*! \brief Gets a list item at index
 *  \param list        A pointer to the list an item contains
 *  \param index       The index of the item
 *
 *  \returns The item it found, NULL if none found
 */
extern void* List_GetI(LIST* list, int index);

/*! \brief gets first item of type
 *  \param list        A pointer to the list an item contains
 *  \param query       The object type to query
 *  \param header      The list item to start searching at 
 */
extern void* List_GetQ(LIST* list, LISTTYPE query, void* header);

/*! \brief Sets a list item
 *  \param list        A pointer to the list an item contains
 *  \param query       The object type to reset
 *  \param object      The new object
 *  \param header      The list item to start searching at 
 */
extern bool List_Reset(LIST* list, LISTTYPE query, void* object, void* header);

/** @} */
#endif
