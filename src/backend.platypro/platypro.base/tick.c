/*
 * Aeden McClain (c) 2016-2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "platypro.base/common.h"
#include "platypro.base/tick.h"

#include <time.h>
#include <math.h>

#ifdef _WIN32
  static LARGE_INTEGER tickTimeScale;
  PRIVATE bool Tick_Update(TICK_OBJ* tickobj)
  {
    if(!tickTimeScale.QuadPart)
      QueryPerformanceFrequency(&tickTimeScale);
    if(!QueryPerformanceCounter(tickobj)) return false;
    return true;
  }

  PRIVATE double getTick(TICK_OBJ tick)
  {
    if(tickTimeScale.QuadPart)
      return (double)tick.QuadPart / (double)tickTimeScale.QuadPart;
    else return 0;
  }

  PRIVATE bool incTick(TICK_OBJ* tick, double amount)
  {
    if(tickTimeScale.QuadPart)
      tick->QuadPart += (amount * tickTimeScale.QuadPart);
    return true;
  }
#else
  #define TICKPOW 1000000000
  PRIVATE bool Tick_Update(TICK_OBJ* tickobj)
  {
    bool result = false;
    if(clock_gettime(CLOCK_MONOTONIC, tickobj) != -1)
    {
      result = true;
    }
    return result;
  }

  PRIVATE double getTick(TICK_OBJ tick)
  {
    double result = 0.0;
    result += (double)tick.tv_nsec / TICKPOW;
    result += tick.tv_sec;
    return result;
  }

  PRIVATE bool incTick(TICK_OBJ* tick, double amount)
  {
    int secs = floor(amount);
    tick->tv_sec += secs;
    amount -= secs;
    tick->tv_nsec += amount * TICKPOW;
    
    if(tick->tv_nsec > TICKPOW)
    {
      tick->tv_nsec -= TICKPOW;
      tick->tv_sec++;
    }
    
    return true;
  }
#endif

PUBLIC int Tick_Get(TICK_OBJ* time)
{
  int result = 0;
  TICK_OBJ newTime;
  Tick_Update(&newTime);
  
  result = ((getTick(newTime) - getTick(*time)) / TICK_LEN);

  incTick(time, (double)result * (double)TICK_LEN);
  return result;
}