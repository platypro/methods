/* 
 * Aeden McClain (c) 2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#ifndef H_APP
#define H_APP

#include <platypro.game/tools.h>
#include <platypro.base/tick.h>
#include <platypro.game/view.h>

#define CANVAS_WIDTH 600
#define CANVAS_HEIGHT 620

#define SETTING_TUTORIAL "tutorial"
#define SETTING_HIGHSCORE "highscore"

#define SETTING_TRUE  "true"
#define SETTING_FALSE "false"

#define COLOR_BACKGROUND (IO_COLOR){0x11,0x11,0x11,0xff}

typedef struct App
{
	LIBSTATE* pLibs;
  TICK_OBJ tick;
  IO_FONT font;
  VIEWMANAGER* views;
  LIST settings;
  LIST strings;
  bool doTutorial;
  uint16_t highScore;

} APP;

#define WINDOW_SIZE (IO_RECT){50,50,CANVAS_WIDTH,CANVAS_HEIGHT}

#endif