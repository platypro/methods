/* 
 * Aeden McClain (c) 2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include <platypro.base/common.h>
#include "app.h"

#include <platypro.game/tools.h>
#include <platypro.game/view.h>
#include <platypro.base/strings.h>
#include <stdlib.h>
#include <cfgpath.h>

#include "view.game.h"
#include "view.mainmenu.h"

int main()
{
  char hitext[10];
  char settingpath[100];
  APP* app = calloc(1, sizeof(APP));
  app->pLibs = Window_New(WINDOW_SIZE, CANVAS_WIDTH, CANVAS_HEIGHT);
  app->views = Views_Init(app);

  get_user_config_file(settingpath, 100, "methods");
  app->font = Draw_LoadFont(app->pLibs, "Fonts/RobotoMono.ttf");
  app->strings  = Strings_Load("methods.str", false);
  app->settings = Strings_Load(settingpath, true);

  app->highScore = atoi(Strings_Get(app->settings, SETTING_HIGHSCORE));

  Tick_Get(&app->tick);

	Views_Create(app->views, "game", sizeof(V_GAME), game_init, game_update, game_draw, game_clean);
	Views_Create(app->views, "mainmenu", sizeof(V_MAINMENU), mainmenu_init, mainmenu_update, mainmenu_draw, mainmenu_clean);
	Views_Set(app->views, "mainmenu");

  Window_Loop(app->pLibs, Views_Update, Views_Draw, app->views);

  snprintf(hitext, 10, "%d", app->highScore);
	Window_Close(&app->pLibs);
  Strings_Set(&app->settings, SETTING_HIGHSCORE, hitext);
  Strings_Write(app->settings, settingpath, "Methods Unconventional Settings");
  free(app);
}
