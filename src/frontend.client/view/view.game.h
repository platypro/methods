/* 
 * Aeden McClain (c) 2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#ifndef H_VIEW_GAME
#define H_VIEW_GAME
#include <app.h>
#include <platypro.game/tools.h>
#include <platypro.base/list.h>

#define TYPE_ENTITY 5
#define TYPE_BULLET 6

#define WAVE_COOLDOWN_PER_ENEMY 50
#define BULLET_COOLDOWN 10
#define IDLE_COOLDOWN 100

#define ENTITY_OFFSCREEN CANVAS_WIDTH / 2

#define ENEMY_SPEED 2
#define TILE_SIZE 40
#define TUTORIAL_HINT_SIZE 50
#define ENEMY_SIZE 15
#define SHOOTER_SIZE  10
#define ENEMY_BOUNDS(e) (IO_RECT){e->p_bake.x, e->p_bake.y, ENEMY_SIZE, ENEMY_SIZE}
#define BULLET_SPACING 30
#define BULLET_SIZE 10
#define BULLET_BOUNDS(e) (IO_RECT){e->p_bake.x, e->p_bake.y, BULLET_SIZE, BULLET_SIZE}

#define COLOR_TUTORIAL_BACK (IO_COLOR){0xff,0x99,0xcc,0xff}
#define COLOR_PLAYER (IO_COLOR){0x8f,0xdb,0x59,0xff}
#define COLOR_SHOOTER (IO_COLOR){0x25,0x4f,0xf7,0xff}
#define COLOR_ARENA (IO_COLOR){0x66,0x66,0x66,0xff}
#define COLOR_BULLET COLOR_RED
#define COLOR_LEVELBAR (IO_COLOR){0xbc,0x57,0x38,0xff}
#define COLOR_TEXT COLOR_WHITE
#define COLOR_ENEMY COLOR_RED

#define MOVE_SCALE 10
#define MOVE_STEP (TILE_SIZE/MOVE_SCALE)
#define ROT_STEP (360/72)

#define ROT_UP    54
#define ROT_DOWN  18
#define ROT_LEFT  36
#define ROT_RIGHT 0

#define PLAYER_RX (TILE_SIZE - 6) / 2
#define PLAYER_RY (TILE_SIZE - 6) / 2

#define SCORE_DAMAGE  -50
#define SCORE_SHOOT   -5
#define SCORE_IDLE     1
#define SCORE_KILL     10

struct v_game;

typedef void (*ENEMYWAVE) (struct v_game* v);

typedef enum Tutorial
{
	TUTORIAL_NONE,
	TUTORIAL_DIRECTION,
	TUTORIAL_SHOOT,
	TUTORIAL_DIAGONAL,
	TUTORIAL_MOVEMENT,
	TUTORIAL_COMPLETE,
} TUTORIAL;

typedef struct Entity_C
{
	uint8_t side       :2;
	uint8_t position   :3;
	DIRECTION direction:3;
	int16_t distance;
} ENTITY_C;

typedef struct Entity
{
	ENTITY_C create;
	//DIRECTION rotation;
	IO_POINT p_bake; //!< Precalculated position
	//uint16_t step;
} ENTITY;

typedef struct Player
{
	IO_POINT p_bake; //!< Precalculated position
	uint8_t posX; //!< Range: 0-128.
	uint8_t posY; //!< Range: 0-128.
	uint8_t rotation; //!< Range: 0-72. Goes in 5 degree increments
} PLAYER;

typedef struct Level
{
	int16_t pointLimit;
	uint8_t waveCooldown;
	uint8_t arenaSize;
	uint8_t tutorial;
	uint8_t waveCount;
	ENEMYWAVE* waves;
} LEVEL;

//Tied to DIRECTION enum
//Allows tutorial to use simple offsets
typedef enum Keymap
{
	KEY_SHOOT,

	KEY_PUP,
	KEY_PLEFT,
	KEY_PDOWN,
	KEY_PRIGHT,

	KEY_MUP,
	KEY_MLEFT,
	KEY_MDOWN,
	KEY_MRIGHT,
} KEYMAP;

typedef struct v_game
{
	//Put view info into here
	uint8_t stickyControl;
	PLAYER player_intent;
	PLAYER player_actual;
	LIST enemies;
	LIST bullets;
	KEYMAP keymap[KEY_MRIGHT + 1];
	uint16_t score;
	LEVEL* level;
	int16_t scoreLeft;
	uint8_t levelAt;
	//Cooldowns
	int16_t waveCooldown;
	int8_t bulletCooldown;
	int8_t idleCooldown;
	//Baked values
	IO_POINT arena_pos;
	char score_bk[35];
	uint16_t level_progress_width;

} V_GAME;


#define GLOB \
  APP* g = (APP*)global;\
  V_GAME* v = (V_GAME*)view;

# define M_PI		3.14159265358979323846

extern char* game_init   (void* global, void* view);
extern char* game_update (void* global, void* view, EVENTSTATE* e);
extern char* game_draw   (void* global, void* view);
extern char* game_clean  (void* global, void* view);
#endif
