/* 
 * Aeden McClain (c) 2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include <platypro.base/common.h>
#include <view.game.h>

#include <platypro.game/tools.h>
#include <platypro.game/view.h>
#include <platypro.base/strings.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

KEYMAP defKeymap[KEY_MRIGHT + 1] =
  {
  GLFW_KEY_SPACE,

  GLFW_KEY_I,
  GLFW_KEY_J,
  GLFW_KEY_K,
  GLFW_KEY_L,

  GLFW_KEY_W,
  GLFW_KEY_A,
  GLFW_KEY_S,
  GLFW_KEY_D,
  };

void pushEntity(ENTITY* entity, int16_t amount)
{
  DIRECTION dir = entity->create.direction;
diagBack:
  switch(dir & 3)
  {
    case DIR_UP:
      entity->p_bake.y -= amount; break;
    case DIR_LEFT:
      entity->p_bake.x -= amount; break;
    case DIR_DOWN:
      entity->p_bake.y += amount; break;
    case DIR_RIGHT:
      entity->p_bake.x += amount; break;
    default:break;
  }
  if(dir & 4)
  {
    dir &= ~4;
    dir = (dir+1)%4;
    goto diagBack;
  }

  entity->create.distance -= amount;
}

bool findSide (V_GAME* v, IO_POINT* point, ENTITY_C* create)
{
  int offset = 0;
  bool result = false;
  if(create->direction & 4)
  {
    offset = TILE_SIZE/2;
    result = true;
  }

  #define sideoffset create->position * TILE_SIZE + (TILE_SIZE/2) - (ENEMY_SIZE / 2)
  #define otherside TILE_SIZE * v->level->arenaSize
  switch (create->side)
  {
    case DIR_UP: // Along top
      point->x += sideoffset;
      point->y -= ENEMY_SIZE - offset;
      break;
    case DIR_RIGHT: // Right Side
      point->x += otherside - offset;
      point->y += sideoffset;
      break;
    case DIR_DOWN: // Bottom
      point->y += otherside - offset;
      point->x += sideoffset;
      break;
    case DIR_LEFT: // Left Side
      point->y += sideoffset;
      point->x -= ENEMY_SIZE - offset;
      break;
    default: break;
  }
  return result;
}

ENTITY* newEnemy (V_GAME* v, ENTITY_C* create)
{
  ENTITY* e = List_Add_Alloc(&v->enemies, sizeof(ENTITY), TYPE_ENTITY);
  e->p_bake = (IO_POINT){v->arena_pos.x, v->arena_pos.y};

  if(findSide(v, &e->p_bake, create)) e->create.distance -= (TILE_SIZE/2);

  e->create = *create;
  int16_t origDistance = e->create.distance;
  e->create.distance = 0;

  v->waveCooldown += WAVE_COOLDOWN_PER_ENEMY;
  
  //Walk enemy away
  pushEntity(e, -ENTITY_OFFSCREEN - origDistance);
  return e;
}

void wave_tripleRapid(V_GAME* v)
{
  ENTITY_C entity = {
    .side = rand()%4,
    .position = rand()%(v->level->arenaSize-1),
    .direction = DIRECTION_OPPOSITE(entity.side),
  };

  for(
    entity.distance = 0; 
    entity.distance <= (2*BULLET_SPACING); 
    entity.distance += BULLET_SPACING)
      newEnemy(v, &entity);
}

void wave_sideswipe(V_GAME* v)
{
  ENTITY_C entity = {
    .side = rand()%4,
    .distance = 0,
    .direction = DIRECTION_OPPOSITE(entity.side),
  };

  for(
    entity.position = 0; 
    entity.position < v->level->arenaSize;
    entity.position ++)
      newEnemy(v, &entity);
}

void wave_diagonal(V_GAME* v)
{
  ENTITY_C entity = {
    .side = rand()%4,
    .distance = 0,
    .position = rand()%v->level->arenaSize,
    .direction = DIRECTION_OPPOSITE(entity.side)
  };
  if(rand()%2 == 1) entity.direction = (entity.direction + 3)%4;
  entity.direction |= 4;

  newEnemy(v, &entity);
}

void wave_fourside(V_GAME* v)
{
  ENTITY_C entity = {
    .position = rand()%v->level->arenaSize,
    .distance = 0,
	.side = 0,
  };

  do
  {
	  entity.direction = DIRECTION_OPPOSITE(entity.side);
	  newEnemy(v, &entity);
	  entity.side++;
	  entity.distance+=(2*BULLET_SPACING);
  } while(entity.side != 0);
}

LEVEL levels[] =
{
  { 40, 50, 1, TUTORIAL_SHOOT, 1, 
    (ENEMYWAVE[]){wave_sideswipe}},
  { 40, 40, 1, TUTORIAL_DIAGONAL, 1,
    (ENEMYWAVE[]){wave_diagonal}},
  { 70, 40, 2, TUTORIAL_MOVEMENT, 1, 
    (ENEMYWAVE[]){wave_sideswipe}},
  { 70, 30, 2, TUTORIAL_COMPLETE, 2, 
    (ENEMYWAVE[]){wave_tripleRapid, wave_sideswipe}},
  {200, 20, 2, TUTORIAL_NONE, 3, 
    (ENEMYWAVE[]){wave_tripleRapid, wave_sideswipe, wave_diagonal}},
  {250, 10, 2, TUTORIAL_NONE, 4,
    (ENEMYWAVE[]){wave_tripleRapid, wave_sideswipe, wave_diagonal, wave_fourside}},
  {250, 10, 3, TUTORIAL_NONE, 2,
    (ENEMYWAVE[]){wave_tripleRapid, wave_sideswipe}},
  {250, 10, 3, TUTORIAL_NONE, 3, (
	ENEMYWAVE[]){wave_tripleRapid, wave_sideswipe, wave_diagonal}},
  {  0, 0, 3, TUTORIAL_NONE, 4, (
    ENEMYWAVE[]){wave_tripleRapid, wave_sideswipe, wave_diagonal, wave_fourside}},
};

void updateArenaPos(V_GAME* v)
{
  v->arena_pos.x = 
    (CANVAS_WIDTH / 2) - (v->level->arenaSize * TILE_SIZE / 2);
  v->arena_pos.y = 
    (CANVAS_HEIGHT / 2) - (v->level->arenaSize * TILE_SIZE / 2);
}

void setLevel(APP* g, V_GAME* v, uint8_t level)
{
  ENTITY* e;
  if(v->enemies && v->level)
  {
    int16_t delta = (v->level->arenaSize - levels[level].arenaSize) * (TILE_SIZE/2);
    foreach(e, v->enemies)
    {
      e->create.distance += delta;
      if((e->create.direction & 4) || e->create.direction == DIR_UP || e->create.direction == DIR_DOWN)
        e->p_bake.x -= delta;
      if((e->create.direction & 4) || e->create.direction == DIR_LEFT || e->create.direction == DIR_RIGHT)
        e->p_bake.y -= delta;
      if(e->create.position > levels[level].arenaSize)
        List_Remove(&v->enemies, e);
    }
  }

  v->level = &levels[level];
  v->levelAt = level;
  if(v->level->tutorial == TUTORIAL_COMPLETE)
    Strings_Set(&g->settings, SETTING_TUTORIAL, SETTING_FALSE); 
  uint8_t newScale = (v->level->arenaSize-1) * MOVE_SCALE;
  if((v->level->arenaSize-1) < (v->player_intent.posX / MOVE_SCALE))
  {
    v->player_intent.posX = newScale;
    v->player_actual.posX = newScale;
  }
  if((v->level->arenaSize-1) < (v->player_intent.posY / MOVE_SCALE))
  {
    v->player_intent.posY = newScale;
    v->player_actual.posY = newScale;
  }
  updateArenaPos(v);
}

void incScore(APP* g, V_GAME* v, int8_t amount)
{
  v->scoreLeft -= amount;
  v->score += amount;
  if(v->scoreLeft < 0 && v->levelAt < (NUMELEMENTS(levels)-1))
  {
    setLevel(g, v, v->levelAt + 1);
    v->score     -= (2*SCORE_SHOOT);
    v->scoreLeft += (2*SCORE_SHOOT);
    v->scoreLeft += v->level->pointLimit;
  }
  else if(v->scoreLeft >= v->level->pointLimit)
  {
    if(v->levelAt)
    {
      v->scoreLeft -= v->level->pointLimit;
      setLevel(g, v, v->levelAt - 1);
    }
    else
    {
      v->score = 0;
      v->scoreLeft = v->level->pointLimit;
    }
  }
  if(v->score > g->highScore) g->highScore = v->score;
  v->level_progress_width = CANVAS_WIDTH - 10;
  if(v->level->pointLimit)
  {
    snprintf(v->score_bk, 35, "%s:%d %s:%d %s:%d",
      Strings_Get(g->strings, "str_LEVEL"), 
      v->levelAt, 
      Strings_Get(g->strings, "str_SCORE"),
      v->score, 
      Strings_Get(g->strings, "str_HIGH_SCORE"),
      g->highScore);
    v->level_progress_width -=
      floor((float)v->scoreLeft / (float)v->level->pointLimit * (float)CANVAS_WIDTH);
  } else
        snprintf(v->score_bk, 35, "%s %s:%d %s:%d",
      Strings_Get(g->strings, "str_LEVEL_MAX"), 
      Strings_Get(g->strings, "str_SCORE"),
      v->score, 
      Strings_Get(g->strings, "str_HIGH_SCORE"),
      g->highScore);
  if(v->score == 0) Views_Set(g->views, "mainmenu");
}

char* game_init   (void* global, void* view)
{ GLOB
  setLevel(g, v, 0);
  v->scoreLeft = v->level->pointLimit;
  incScore(g, v, 1);
  memcpy(v->keymap, defKeymap, sizeof(defKeymap));

  updateArenaPos(v);

  srand((unsigned) time(NULL));
  
  return VIEW_CONTINUE;
}

DIRECTION getDir(uint8_t rotation)
{
  return
    rotation == 0  ? DIR_LEFT:
    rotation <  18 ? DIR_UPLEFT:
    rotation == 18 ? DIR_UP:
    rotation <  36 ? DIR_RIGHTUP:
    rotation == 36 ? DIR_RIGHT:
    rotation <  54 ? DIR_DOWNRIGHT:
    rotation == 54 ? DIR_DOWN:
                     DIR_LEFTDOWN;
}

char* game_update (void* global, void* view, EVENTSTATE* e)
{ GLOB
  ENTITY* entity, *bullet;
  int ticks = Tick_Get(&g->tick);

  //Update intent
  if(glfwGetKey(g->pLibs->window, v->keymap[KEY_MUP]) == GLFW_PRESS
    && v->player_intent.posY > 0
    && v->player_intent.posY >= v->player_actual.posY)
      v->player_intent.posY -= MOVE_SCALE;

  if(glfwGetKey(g->pLibs->window, v->keymap[KEY_MLEFT]) == GLFW_PRESS
    && v->player_intent.posX > 0
    && v->player_intent.posX >= v->player_actual.posX)
      v->player_intent.posX -= MOVE_SCALE;
  
  if(glfwGetKey(g->pLibs->window, v->keymap[KEY_MDOWN]) == GLFW_PRESS
    && v->player_intent.posY < (v->level->arenaSize-1) * MOVE_SCALE
    && v->player_intent.posY <= v->player_actual.posY)
      v->player_intent.posY += MOVE_SCALE;
  
  if(glfwGetKey(g->pLibs->window, v->keymap[KEY_MRIGHT]) == GLFW_PRESS
    && v->player_intent.posX < (v->level->arenaSize-1) * MOVE_SCALE
    && v->player_intent.posX <= v->player_actual.posX)
      v->player_intent.posX += MOVE_SCALE;

  if(glfwGetKey(g->pLibs->window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    Views_Set(g->views, "mainmenu");

  uint8_t keymap = 
    (glfwGetKey(g->pLibs->window, v->keymap[KEY_PUP]) == GLFW_PRESS) << 0 |
    (glfwGetKey(g->pLibs->window, v->keymap[KEY_PLEFT]) == GLFW_PRESS) << 1 |
    (glfwGetKey(g->pLibs->window, v->keymap[KEY_PDOWN]) == GLFW_PRESS) << 2 |
    (glfwGetKey(g->pLibs->window, v->keymap[KEY_PRIGHT]) == GLFW_PRESS) << 3 ;
  v->player_intent.rotation =
    keymap == 1  ? ROT_UP:        //54
    keymap == 2  ? ROT_LEFT:      //36
    keymap == 3  ? ROT_LEFT  + 9: //45
    keymap == 4  ? ROT_DOWN:      //18
    keymap == 6  ? ROT_DOWN  + 9: //27
    keymap == 8  ? ROT_RIGHT:     //0
    keymap == 9  ? ROT_UP    + 9: //63
    keymap == 12 ? ROT_RIGHT + 9: //9
    v->player_intent.rotation;
    //1000 1100 0100 0110 0010 0011 0001

  if(glfwGetKey(g->pLibs->window, v->keymap[KEY_SHOOT]) 
    == GLFW_PRESS && !v->bulletCooldown)
  {
    ENTITY* bullet = List_Add_Alloc(&v->bullets, sizeof(ENTITY), TYPE_BULLET);
    bullet->p_bake.x = v->player_actual.p_bake.x - (BULLET_SIZE/2);
    bullet->p_bake.y = v->player_actual.p_bake.y - (BULLET_SIZE/2);
    bullet->create.direction = getDir(v->player_intent.rotation);
    v->bulletCooldown = BULLET_COOLDOWN;
  }

  if(ticks)
  {
  if(v->idleCooldown > 0)
    v->idleCooldown -= ticks;
  else
  {
    v->idleCooldown = IDLE_COOLDOWN;
    incScore(g, v, SCORE_IDLE);
  }

  if(v->waveCooldown > 0)
    v->waveCooldown -= ticks;
  else
  {
    if(v->level->waveCount)
      v->level->waves[rand() % v->level->waveCount](v);
    v->waveCooldown += v->level->waveCooldown;
  }

  if(v->bulletCooldown > 0)
    v->bulletCooldown -= ticks;


  v->player_actual.p_bake.x = 
    v->arena_pos.x + (v->player_actual.posX * MOVE_STEP) + 3 + PLAYER_RX;
  v->player_actual.p_bake.y =
    v->arena_pos.y + (v->player_actual.posY * MOVE_STEP) + 3 + PLAYER_RY;

  //Animate x position
  if(v->player_actual.posX < v->player_intent.posX) 
    v->player_actual.posX ++;
  else if(v->player_actual.posX > v->player_intent.posX) 
    v->player_actual.posX --;

  //Animate y position
  if(v->player_actual.posY < v->player_intent.posY) 
    v->player_actual.posY ++;
  else if(v->player_actual.posY > v->player_intent.posY) 
    v->player_actual.posY --;

  //Animate rotation
  if(v->player_actual.rotation != v->player_intent.rotation)
  {
    int diff2 = v->player_intent.rotation - v->player_actual.rotation;
    if(diff2 < 0) diff2 += 72;

    int diff1 = v->player_actual.rotation - v->player_intent.rotation;
    if(diff1 < 0) diff1 += 72;
    
    if(diff2 <= 36) 
      v->player_actual.rotation = 
        (v->player_actual.rotation + 3) % 72;
    else if(diff1 < 36) 
      v->player_actual.rotation =
        (72+v->player_actual.rotation - 3) % 72;
  }

  //Animate enemies
  foreach(entity, v->enemies)
  {
    pushEntity(entity, ENEMY_SPEED);
    if(entity->create.distance <= 0)
    {
      List_Remove(&v->enemies, entity);
      incScore(g, v, SCORE_DAMAGE);
    }
  }

  //Animate bullets
  {
  foreach(bullet, v->bullets)
  {
    pushEntity(bullet, -5);

    if(bullet->create.distance > ENTITY_OFFSCREEN)
    {
      List_Remove(&v->bullets, bullet);
      incScore(g, v, SCORE_SHOOT);
    }
    else
    {
      //Enemy-bullet collision
      foreach(entity, v->enemies)
      {
        IO_RECT r1 = ENEMY_BOUNDS(entity);
        IO_RECT r2 = BULLET_BOUNDS(bullet);
        if(testRectCollision(&r1, &r2))
        {
          incScore(g, v, SCORE_KILL);
          List_Remove(&v->bullets, bullet);
          List_Remove(&v->enemies, entity);
        }
      }
    }
  }
  }
  }

  return VIEW_CONTINUE;
}

PRIVATE void drawHint(APP* g, int x, int y, char code)
{
  Draw_Color(g->pLibs, COLOR_TUTORIAL_BACK);
  nvgBeginPath(g->pLibs->draw);
  nvgRoundedRect(g->pLibs->draw, 
    x - (code != GLFW_KEY_SPACE ? TUTORIAL_HINT_SIZE / 2 : TUTORIAL_HINT_SIZE*2),
    y - (TUTORIAL_HINT_SIZE/2),
    code != GLFW_KEY_SPACE ? TUTORIAL_HINT_SIZE : 4*TUTORIAL_HINT_SIZE, 
    TUTORIAL_HINT_SIZE, 5);
  nvgFill(g->pLibs->draw);

  Draw_Color(g->pLibs, COLOR_BLACK);
  Draw_Text(g->pLibs, g->font, (IO_POINT){x,y}, TUTORIAL_HINT_SIZE, 
    code != GLFW_KEY_SPACE ? (char[]){code, '\0'} : "Space");
}

#define TUT_LO(a) a/2-a/5
#define TUT_HI(a)  a/2+a/5
#define TUT_ON(a)    a/2
#define TUT_LEFT_X  TUT_LO(CANVAS_WIDTH)
#define TUT_LEFT_Y  TUT_ON(CANVAS_HEIGHT)
#define TUT_UP_X    TUT_ON(CANVAS_WIDTH)
#define TUT_UP_Y    TUT_LO(CANVAS_HEIGHT)
#define TUT_RIGHT_X TUT_HI(CANVAS_WIDTH)
#define TUT_RIGHT_Y TUT_ON(CANVAS_HEIGHT)
#define TUT_DOWN_X  TUT_ON(CANVAS_WIDTH)
#define TUT_DOWN_Y  TUT_HI(CANVAS_HEIGHT)
#define TUT_OFFSET  (TUTORIAL_HINT_SIZE + 5)

PRIVATE inline void tutorial_draw(APP* g, V_GAME* v)
{
  nvgTextAlign(g->pLibs->draw,NVG_ALIGN_CENTER|NVG_ALIGN_MIDDLE);
  switch(v->level->tutorial)
  {
    case TUTORIAL_SHOOT: // Shooting
      drawHint(g, TUT_LEFT_X, TUT_LEFT_Y, v->keymap[KEY_PLEFT]);
      drawHint(g, TUT_RIGHT_X, TUT_RIGHT_Y, v->keymap[KEY_PRIGHT]);
      drawHint(g, TUT_UP_X, TUT_UP_Y, v->keymap[KEY_PUP]);
      drawHint(g, TUT_DOWN_X, TUT_DOWN_Y, v->keymap[KEY_PDOWN]);
      drawHint(g, 
        TUT_DOWN_X, TUT_DOWN_Y + (2*TUTORIAL_HINT_SIZE), v->keymap[KEY_SHOOT]);
      break;
    case TUTORIAL_DIAGONAL: // Diagonal pointing
      drawHint(g, TUT_LEFT_X - TUT_OFFSET,  
        TUT_UP_Y,   v->keymap[KEY_PLEFT]);
      drawHint(g, TUT_LEFT_X,  
        TUT_UP_Y,   v->keymap[KEY_PUP]);
      drawHint(g, TUT_RIGHT_X, 
        TUT_UP_Y,   v->keymap[KEY_PRIGHT]);
      drawHint(g, TUT_RIGHT_X + TUT_OFFSET, 
        TUT_UP_Y,   v->keymap[KEY_PUP]);
      drawHint(g, TUT_LEFT_X,  
        TUT_DOWN_Y, v->keymap[KEY_PLEFT]);
      drawHint(g, TUT_LEFT_X - TUT_OFFSET,  
        TUT_DOWN_Y, v->keymap[KEY_PDOWN]);
      drawHint(g, TUT_RIGHT_X, 
        TUT_DOWN_Y, v->keymap[KEY_PRIGHT]);
      drawHint(g, TUT_RIGHT_X + TUT_OFFSET, 
        TUT_DOWN_Y, v->keymap[KEY_PDOWN]);
      break;
    case TUTORIAL_MOVEMENT: // Movement
      drawHint(g, TUT_LEFT_X, TUT_LEFT_Y, v->keymap[KEY_MLEFT]);
      drawHint(g, TUT_RIGHT_X, TUT_RIGHT_Y, v->keymap[KEY_MRIGHT]);
      drawHint(g, TUT_UP_X, TUT_UP_Y, v->keymap[KEY_MUP]);
      drawHint(g, TUT_DOWN_X, TUT_DOWN_Y, v->keymap[KEY_MDOWN]);
      break;
    default:
      break;
  }
}

PRIVATE inline void player_draw(LIBSTATE* libs, PLAYER* player)
{
  float angle = player->rotation * ROT_STEP * (M_PI/180.0f);

  IO_RECT shooter = (IO_RECT) {
      (player->p_bake.x + PLAYER_RX * cos(angle)) - SHOOTER_SIZE/2,
      (player->p_bake.y + PLAYER_RY * sin(angle)) - SHOOTER_SIZE/2,
      SHOOTER_SIZE, SHOOTER_SIZE,
  };

  Draw_Color(libs, COLOR_PLAYER);
  nvgBeginPath(libs->draw);
  nvgEllipse(libs->draw,
          player->p_bake.x,
          player->p_bake.y,
          PLAYER_RX, PLAYER_RY);
  nvgFill(libs->draw);
  Draw_Color(libs, COLOR_SHOOTER);
  Draw_Rect(libs, shooter);
}

char* game_draw   (void* global, void* view)
{ GLOB
  ENTITY* e;

  //(0) Draw backdrop
  Draw_Color(g->pLibs, COLOR_BACKGROUND);
  Draw_Rect(g->pLibs, (IO_RECT){0,0,CANVAS_WIDTH,CANVAS_HEIGHT});

  nvgScissor(g->pLibs->draw, 0, 20, 600, 600);

  //(1) Draw Arena
  Draw_Color(g->pLibs, COLOR_ARENA);
  //calculate area taken
  int i,j,cx,cy;
  for(
    i = 0, cx = v->arena_pos.x; 
    i < v->level->arenaSize; 
    i++, cx += TILE_SIZE)
  for(
    j = 0, cy = v->arena_pos.y; 
    j < v->level->arenaSize; 
    j++, cy += TILE_SIZE)
  Draw_Rect(g->pLibs, ((IO_RECT){cx+1,cy+1,TILE_SIZE-2,TILE_SIZE-2}));
  
  //(2) Draw Tutorial
  if(!strcmp(Strings_Get(g->settings, SETTING_TUTORIAL), SETTING_TRUE))
    tutorial_draw(g, v);

  //(3) Draw bullets
  Draw_Color(g->pLibs, COLOR_SHOOTER);
  foreach(e, v->bullets)
  {
    Draw_Round(g->pLibs, BULLET_BOUNDS(e));
  }

  //(4) Draw player
  player_draw(g->pLibs, &v->player_actual);

  //(5) Draw enemies
  {
  Draw_Color(g->pLibs, COLOR_ENEMY);
  foreach(e, v->enemies)
  {
    Draw_Rect(g->pLibs, ENEMY_BOUNDS(e));
  }}

  nvgScissor(g->pLibs->draw, 0, 0, 600, 30);
  //(5) Draw title
  if(v->level_progress_width)
  {
    Draw_Color(g->pLibs, COLOR_LEVELBAR);
    nvgBeginPath(g->pLibs->draw);
    nvgRoundedRect(g->pLibs->draw, 5, 5, v->level_progress_width, 20, 5);
    nvgFill(g->pLibs->draw);
  }
  Draw_Color(g->pLibs, COLOR_TEXT);
  nvgTextAlign(g->pLibs->draw, NVG_ALIGN_LEFT | NVG_ALIGN_BASELINE);
  Draw_Text(g->pLibs, g->font, (IO_POINT){12,21}, 24, v->score_bk);

  return VIEW_CONTINUE;
}

char* game_clean  (void* global, void* view)
{ GLOB
  
  return VIEW_CONTINUE;
}
