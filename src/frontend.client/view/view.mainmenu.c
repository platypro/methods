/*
 * Aeden McClain (c) 2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <platypro.base/common.h>
#include <view.mainmenu.h>

#include <platypro.game/tools.h>
#include <platypro.game/view.h>
#include <platypro.base/strings.h>
#include <string.h>

UI_STYLE uiStyle = {
    (IO_COLOR){0xBE,0xBE,0xDD,0xFF},
    (IO_COLOR){0x45,0x45,0x99,0xFF},
    (IO_COLOR){0xFF,0xFF,0xFF,0xFF},
    (IO_COLOR){0x00,0x00,0x99,0xFF},
    (IO_COLOR){0x00,0x00,0x66,0xFF},
    (IO_COLOR){0xBF,0xBF,0xBF,0xFF}
};

int ClickEvent_start(UI_INDEX* index, UI_ELEMENT* object, void* viewdata)
{
  APP* state = (APP*)viewdata;
  Views_Set(state->views, "game");
}

int ClickEvent_setTutorial(UI_INDEX* index, UI_ELEMENT* object, void* viewdata)
{
  APP* state = (APP*)viewdata;
  state->doTutorial = !strcmp(object->text, UI_TRUE);
  Strings_Set(&state->settings,"tutorial", 
    state->doTutorial ? SETTING_TRUE : SETTING_FALSE);
}

int ClickEvent_quit(UI_INDEX* index, UI_ELEMENT* object, void* viewdata)
{
  APP* state = (APP*)viewdata;
  Views_Set(state->views, VIEW_BREAK);
}

#define BUTTON_SPACING (BUTTON_HEIGHT + 5)
PRIVATE UI_DEF menuButtons[] =
{
  {
    "", 
    UITYPE_BTN, 
    "str_START_GAME", 
    (IO_RECT)
    {
      CANVAS_WIDTH/2-(BUTTON_WIDTH/2),
      CANVAS_HEIGHT/2,
      BUTTON_WIDTH,
      BUTTON_HEIGHT
    }, 
    ClickEvent_start
  },
  {
    "", 
    UITYPE_LABEL, 
    "str_SET_TUTORIAL", 
    (IO_RECT)
    {
      CANVAS_WIDTH/2-(BUTTON_WIDTH/2)+BUTTON_HEIGHT-10,
      CANVAS_HEIGHT/2+BUTTON_SPACING,
      BUTTON_WIDTH-BUTTON_HEIGHT+10,
      BUTTON_HEIGHT
    }, 
    NULL
  },
  {
    UINAME_SET_TUTORIAL, 
    UITYPE_CHECK, 
    UI_TRUE, 
    (IO_RECT)
    {
      CANVAS_WIDTH/2-(BUTTON_WIDTH/2),
      CANVAS_HEIGHT/2+BUTTON_SPACING,
      BUTTON_HEIGHT,
      BUTTON_HEIGHT
    },  
    ClickEvent_setTutorial
  },
  {
    "", 
    UITYPE_BTN, 
    "str_QUIT", 
    (IO_RECT)
    {
      CANVAS_WIDTH/2-(BUTTON_WIDTH/2),
      CANVAS_HEIGHT/2+(2*BUTTON_SPACING),
      BUTTON_WIDTH,
      BUTTON_HEIGHT
    }, 
    ClickEvent_quit
  },
};

char* mainmenu_init   (void* global, void* view)
{ GLOB
  v->ui = UI_Create(g->pLibs, &uiStyle, g->font);
  snprintf(v->highScore,20,"%s:%d", 
    Strings_Get(g->strings, "str_HIGH_SCORE"), g->highScore);
  char* tut;
  if((tut = Strings_GetValue(g->settings, SETTING_TUTORIAL)))
  {
    if(!strcmp(tut, SETTING_FALSE))
      menuButtons[2].text = UI_FALSE;
  } else Strings_Set(&g->settings, SETTING_TUTORIAL, SETTING_TRUE);
  g->doTutorial = !strcmp(menuButtons[2].text, UI_TRUE);

  UI_Object_Auto(v->ui, menuButtons, sizeof(menuButtons), g->strings);
	return VIEW_CONTINUE;
}

char* mainmenu_update (void* global, void* view, EVENTSTATE* e)
{ GLOB
  UI_Update(v->ui, *e, g);
	return VIEW_CONTINUE;
}

char* mainmenu_draw   (void* global, void* view)
{ GLOB
  // Draw backdrop
  Draw_Color(g->pLibs, COLOR_BACKGROUND);
  Draw_Rect(g->pLibs, (IO_RECT){0,0,CANVAS_WIDTH,CANVAS_HEIGHT});

  Draw_Color(g->pLibs, COLOR_WHITE);
  nvgBeginPath(g->pLibs->draw);
  nvgTextAlign(g->pLibs->draw, NVG_ALIGN_CENTER | NVG_ALIGN_MIDDLE);
  nvgFontSize(g->pLibs->draw, 45);
  nvgFontFace(g->pLibs->draw, g->font);
  nvgTextBox(g->pLibs->draw, 0, CANVAS_HEIGHT/4, CANVAS_WIDTH, "Methods\nUnconventional", NULL);
  nvgBeginPath(g->pLibs->draw);
  nvgFontSize(g->pLibs->draw, 20);
  nvgTextBox(g->pLibs->draw, 0, CANVAS_HEIGHT/2-20, CANVAS_WIDTH, v->highScore, NULL);
  UI_Draw(v->ui);
	return VIEW_CONTINUE;
}

char* mainmenu_clean  (void* global, void* view)
{ GLOB
  UI_Destroy(&v->ui);
  glfwGetKey(g->pLibs->window, GLFW_KEY_ESCAPE);
	return VIEW_CONTINUE;
}
