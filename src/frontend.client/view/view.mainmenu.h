/*
 * Aeden McClain (c) 2018
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_VIEW_MAINMENU
#define H_VIEW_MAINMENU

#include <app.h>
#include <platypro.game/tools.h>
#include <platypro.game/ui.h>

#define UINAME_SET_TUTORIAL "set_tutorial"

#define BUTTON_WIDTH 300
#define BUTTON_HEIGHT 50

typedef struct v_mainmenu
{
  UI_INDEX* ui;
  char highScore[20];

} V_MAINMENU;

#ifndef GLOB
#define GLOB \
  APP* g = (APP*)global;\
  V_MAINMENU* v = (V_MAINMENU*)view;
#endif

extern char* mainmenu_init   (void* global, void* view);
extern char* mainmenu_update (void* global, void* view, EVENTSTATE* e);
extern char* mainmenu_draw   (void* global, void* view);
extern char* mainmenu_clean  (void* global, void* view);

#endif
